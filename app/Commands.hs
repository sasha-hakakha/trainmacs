module Commands where

type Command = String

type Arg = String

type Flags = [String]

data Input = Input Command Arg Flags

showConnections :: [String] -> IO ()
showConnections flags
  | length flags > 1 = putStrLn "too many flags for showing connections"
  | length flags == 0 = putStrLn "all da flags"
  | otherwise = putStrLn $ "all da flags containing" ++ (head flags)

handleShow :: Arg -> Flags -> IO ()
handleShow "connections" flags = showConnections flags
handleShow x _ = putStrLn $ x ++ " is an invalid argument for show"

handleInvalidCommand :: Command -> IO ()
handleInvalidCommand x = putStrLn $ "invalid command: " ++ x

handleCommand :: String -> IO ()
handleCommand input = case words input of
  ("show" : cmd : args) -> handleShow cmd args
  [] -> putStrLn ""
  (x : _) -> handleInvalidCommand $ x
