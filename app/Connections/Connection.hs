module Connections.Connection where

import Places.Place (Place)

type Connection = (Place, Place)
