module GameTime where

import Data.Time.Clock.POSIX (getPOSIXTime)

getRoundedUnixTime :: IO Integer
getRoundedUnixTime = do
  posixTime <- getPOSIXTime
  return $ round (100 * posixTime)

-- return $ round (100 * posixTime) `div` 100

getMostRecentTimeDiff :: [Integer] -> Integer
getMostRecentTimeDiff times = case reverse times of
  x : y : _ -> x - y
  _ -> 0
