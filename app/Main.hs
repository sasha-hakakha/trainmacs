import Commands (handleCommand)
import Connections.Connection (Connection)
import GameTime (getMostRecentTimeDiff, getRoundedUnixTime)
import Paths.Path (Path)
import Places.GeneratePlaces (generatePlaces)
import Places.Place (Place)
import System.IO (hFlush, stdout)

data GameState = GameState
  { tick :: Integer,
    logs :: [String],
    timeStamps :: [Integer],
    paths :: [Path],
    connections :: [Connection]
  }

updateGameState :: GameState -> Integer -> String -> GameState
updateGameState gameState newTimeStamp userInput =
  gameState
    { tick = newTick,
      logs = logs gameState ++ [newLog],
      timeStamps = timeStamps gameState ++ [newTimeStamp],
      paths = paths gameState,
      connections = connections gameState
    }
  where
    newTick = tick gameState + 1
    newLog = show $ getMostRecentTimeDiff (timeStamps gameState)

gameLoop :: GameState -> IO ()
gameLoop gameState = do
  putStr "₮ "
  hFlush stdout
  userInput <- getLine
  currentTimeStamp <- getRoundedUnixTime

  if userInput == "quit"
    then putStrLn "Exiting..."
    else do
      handleCommand userInput
      let newGameState = updateGameState gameState currentTimeStamp userInput
      gameLoop newGameState

main :: IO ()
main = do
  initialState <- GameState 0 ["new world initialized successfully!"] . pure <$> getRoundedUnixTime [] []
  gameLoop initialState
