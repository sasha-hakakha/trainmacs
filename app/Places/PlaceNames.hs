-- I know this is ugly. I do not care
module Places.PlaceNames where

import System.Random (randomRIO, newStdGen)
import System.Random.Shuffle (shuffle')
import Data.List (nub, (\\), delete)

placeNames :: [String]
placeNames =
  [ "Seakarta",
    "Fauxfield",
    "Richhampton",
    "Appleham",
    "Aelview",
    "Bannton",
    "Backcaster",
    "Bayburg",
    "Seamouth",
    "Gilkarta",
    "Kettleview",
    "Appleworth",
    "Greenland",
    "Richham",
    "Eggdol",
    "Hardfield",
    "Julside",
    "Readingwich",
    "Beachness",
    "Redbury",
    "Lunadol",
    "Factville",
    "Strongdol",
    "Eldale",
    "Clamburg",
    "Kingland",
    "Baydol",
    "Strongview",
    "Passcaster",
    "Hardside",
    "Casterstead",
    "Tallmouth",
    "Snowhampton",
    "Hoghampton",
    "Tallport",
    "Clamley",
    "Eastby",
    "Princeview",
    "Manswich",
    "Julcester",
    "Milltown",
    "Hamtown",
    "Duckdale",
    "Lunfolk",
    "Greencaster",
    "Seaworth",
    "Frostdol",
    "Bayfield",
    "Fauxley",
    "Sweetwich",
    "Foxby",
    "Weirdol",
    "Sagegrad",
    "Bridgeley",
    "Transwich",
    "Mannorham",
    "Rockside",
    "Lunbrough",
    "Cloudfield",
    "Melgrad",
    "Roseburgh",
    "Oakpool",
    "Sandburg",
    "Melcester",
    "Sageworth",
    "Dayburg",
    "Tallham"
  ]

getRandomPlaceNames :: Int -> IO [String]
getRandomPlaceNames amount = do
  gen <- newStdGen
  let shuffled = shuffle' placeNames (length placeNames) gen
  return $ take (amount - 1) shuffled ++ ["Portland"]
