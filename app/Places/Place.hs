module Places.Place where

import Data.List (find)

data Place = Place
  { name :: String,
    x_coord :: Double,
    y_coord :: Double
  }
  deriving (Show)

findPlaceByName :: String -> [Place] -> Maybe Place
findPlaceByName target = find (\place -> name place == target)
