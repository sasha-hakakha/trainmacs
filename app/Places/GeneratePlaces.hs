module Places.GeneratePlaces where

import Places.GeneratePlaceCoords (Point (..), generatePoints)
import Places.Place (Place (..))
import Places.PlaceNames (getRandomPlaceNames)

createPlaces :: [String] -> [(Double, Double)] -> [Place]
createPlaces names points = map (\(name, (x, y)) -> Place {name = name, x_coord = x, y_coord = y}) (zip names points)

generatePlaces :: Int -> Double -> Double -> Double -> IO [Place]
generatePlaces amount width height minDist = do
  points <- generatePoints width height amount minDist
  names <- getRandomPlaceNames amount
  return $ createPlaces names points
