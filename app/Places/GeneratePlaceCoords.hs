module Places.GeneratePlaceCoords where

import Control.Monad (replicateM)
import System.Random (randomRIO)
import Control.Exception (assert)

type Point = (Double, Double)

allTrue :: [Bool] -> Bool
allTrue = all id

distTwoPoints :: Point -> Point -> Double
distTwoPoints (x1, y1) (x2, y2) = sqrt ((x2 - x1) ^ 2 + (y2 - y1) ^ 2)

distAboveMin :: Point -> Point -> Double -> Bool
distAboveMin a b minDist = distTwoPoints a b > minDist

pointValidlySpaced :: Point -> [Point] -> Double -> Bool
pointValidlySpaced point points minDist = allTrue $ map (\p -> distAboveMin point p minDist) points

generatePoints :: Double -> Double -> Int -> Double -> IO [Point]
generatePoints width height amount minDist = assert (amount <= maxNumPoints) go [] amount
  where
    maxNumPoints = maxPoints width height minDist

    go :: [Point] -> Int -> IO [Point]
    go points 0 = return points
    go points n = do
      newPoint <- generateRandomPoint width height
      if pointValidlySpaced newPoint points minDist
        then go (newPoint : points) (n - 1)
        else go points n

generateRandomPoint :: Double -> Double -> IO Point
generateRandomPoint width height = do
  x <- randomRIO (0.0, width)
  y <- randomRIO (0.0, height)
  return (x, y)

-- check feasible
maxPoints :: Double -> Double -> Double -> Int
maxPoints width height minDist = floor $ (2 * area) / (sqrt 3 * diameter ^ 2)
  where
    area = width * height
    diameter = minDist
