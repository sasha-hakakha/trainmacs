module Vehicles.VehicleLocation where

import Places.Place (Place)

data Location = AtPlace Place | InTransit 
  { origin :: Place,
    destination :: Place,
    distanceFromDestination :: Double
  }
