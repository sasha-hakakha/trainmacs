-- TODO test
module Paths.Path where

import Data.List.Split (splitOn)
import Places.Place (Place, findPlaceByName)

type Path = [Place]

connectionExists :: (Eq a) => a -> a -> [(a, a)] -> Bool
connectionExists first second connections = (first, second) `elem` connections || (second, first) `elem` connections

allConnectionsExist :: (Eq a) => [a] -> [(a, a)] -> Bool
allConnectionsExist path connections = all (\(x, y) -> connectionExists x y connections) pairs
  where
    pairs = zip path (tail path)

pathIsValid :: (Eq a) => [a] -> [(a, a)] -> Bool
pathIsValid path connections =
  length path > 1
    && allConnectionsExist path connections
    && allDifferent pairs
    && head path == last path
  where
    pairs = zip path (tail path)
    allDifferent [] = True
    allDifferent ((x, y) : rest) = x /= y && allDifferent rest

pathFromString :: String -> [Place] -> Maybe [Place]
pathFromString input places =
  sequence $ map (`findPlaceByName` places) placeStringList
  where
    placeStringList = splitOn "," input
